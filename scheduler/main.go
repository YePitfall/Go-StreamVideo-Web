package main

import "GoStreamVideoWebsite/scheduler/taskrunner"

func main() {
	go taskrunner.Start()
	r := RegisterHandlers()
	r.Run(":9001")

}
