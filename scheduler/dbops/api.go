package dbops

//1.api ->videoid->mysql

//2.api service ->scheduler ->write video  deletion record
//3. timer
//4 timer -> runner ->read (write video  deletion record) ->delete video from folder

func AddVideoDeletionRecord(vid string) error {
	var vds VideoDelRec
	vds.VideoId = vid
	if err := dbConn.Create(&vds).Error; err != nil {
		return err
	}
	return nil
}
