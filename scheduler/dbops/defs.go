package dbops

const (
	VIDEO_PATH   = "./videos/"
	VIDEO_SUFFIX = ".mp4"
)

type VideoDelRec struct {
	VideoId string `json:"video_id" gorm:"type:varchar(64);primary_key"`
}
