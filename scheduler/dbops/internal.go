package dbops

import "os"

func ReadVideoDeletionRecord(count int) ([]VideoDelRec, error) {
	//dbConn.Order("created_at desc").Offset((pageNum-1)*pageSize).Limit(pageSize).Find(&posts)
	var vds []VideoDelRec
	if err := dbConn.Limit(count).Find(&vds).Error; err != nil {
		return nil, err
	}
	return vds, nil
}
func DelVideoDeletionRecord(vid string) error {
	if err := dbConn.Delete(VideoDelRec{}, vid).Error; err != nil {
		return err
	}
	RemoveVideoFile(vid)
	return nil
}
func RemoveVideoFile(vid string) error {
	err := os.Remove(VIDEO_PATH + vid + VIDEO_SUFFIX)
	if err != nil && !os.IsNotExist(err) { //如果不是 文件不存在的错误 返回
		return err
	}
	return nil
}
