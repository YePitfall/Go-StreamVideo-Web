package main

import "github.com/gin-gonic/gin"

func sendResponse(c *gin.Context, sc int, resp string) {
	c.IndentedJSON(sc, resp)
}
