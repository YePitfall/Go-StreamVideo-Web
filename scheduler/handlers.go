package main

import (
	"GoStreamVideoWebsite/scheduler/dbops"
	"github.com/gin-gonic/gin"
)

func RegisterHandlers() *gin.Engine {
	r := gin.Default()
	r.GET("/video-delete-record/:vid-id", vidDelRecHandler)
	return r
}
func vidDelRecHandler(c *gin.Context) {
	vid := c.Param("vid-id")
	if len(vid) == 0 {
		sendResponse(c, 400, "video id should not be empty")
		return
	}
	err := dbops.AddVideoDeletionRecord(vid)
	if err != nil {
		sendResponse(c, 500, "Internal server error")
		return
	}
	c.IndentedJSON(200, "success")
}
