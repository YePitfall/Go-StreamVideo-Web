package taskrunner

import "time"

type Worker struct {
	ticker *time.Ticker //定时器
	runner *Runner
}

func NewWorker(interval time.Duration, r *Runner) *Worker {
	return &Worker{
		ticker: time.NewTicker(interval * time.Second),
		runner: r,
	}
}
func (w *Worker) StartWorker() {
	for {
		select {
		case <-w.ticker.C: //隔一段时间执行 下面的函数，注意这里不要用for range来取ticker channel的值 会有误差
			go w.runner.StartAll()
		}
	}
}

func Start() {
	// Start video file cleaning
	r := NewRunner(3, true, VideoClearDispatcher, VideoClearExecutor)
	w := NewWorker(10, r)
	go w.StartWorker() //在 main.go文件要调用这个函数 ，然后还要select，否则这里还没执行就结束了
}
