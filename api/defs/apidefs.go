package defs

import "time"

//request
type UserCredential struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

//data model
type Users struct {
	ID       uint   `gorm:"primary_key"json:"id"`
	Username string `gorm:"type:varchar(64);not null;unique" json:"username"`
	Password string `gorm:"type:text" json:"password"`
}
type Video_Info struct {
	ID           string `json:"id" gorm:"type:varchar(64);primary_key"`
	AuthorID     uint   `json:"author_id" gorm:"type:int;not null"`
	Name         string `json:"name" gorm:"type:text;not null"`
	DisplayCtime string `json:"display_ctime" gorm:"type:text;not null"`
	CreatedAt    time.Time
}
type Comments struct {
	ID        string `json:"id" gorm:"type:varchar(64);primary_key"`
	VideoID   string `json:"video_id" gorm:"type:varchar(64);not null"`
	AuthorID  uint   `json:"author_id" gorm:"type:int;not null"`
	Content   string `json:"content" gorm:"type:text;not null"`
	CreatedAt time.Time
}
type CommentWithName struct {
	ID       string `json:"id" gorm:"type:varchar(64);primary_key"`
	Username string `json:"username" gorm:"type:varchar(64);not null"`
	Content  string `json:"content" gorm:"type:text;not null"`
}
type Sessions struct {
	SessionId string `json:"session_id" gorm:"type:varchar(64);primary_key"`
	TTL       string `json:"ttl" gorm:"type:tinytext;not null"`
	Username  string `json:"username" gorm:"type:varchar(64);not null"`
}
type SimpleSession struct {
	Username string
	TTL      int64
}
type SignedUp struct {
	Success   bool   `json:"success"`
	SessionID string `json:"session_id"`
}
