package main

import (
	"GoStreamVideoWebsite/api/dbops"
	"GoStreamVideoWebsite/api/defs"
	"GoStreamVideoWebsite/api/session"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"log"
)

func RegisterRouter() *gin.Engine {
	r := gin.Default()
	r.POST("/user", CreateUser)
	r.GET("/user/:id", Login)

	return r
}

func Login(c *gin.Context) {

}
func CreateUser(c *gin.Context) {
	res, _ := ioutil.ReadAll(c.Request.Body)
	log.Println(string(res))
	ubody := defs.UserCredential{}
	if err := json.Unmarshal(res, &ubody); err != nil {
		sendErrorResponse(c, defs.ErrorRequestBodyParseFailed)
		return
	}
	if err := dbops.AddUserCredential(ubody.Username, ubody.Password); err != nil {
		sendErrorResponse(c, defs.ErrorDBError)
		return
	}
	id := session.GenerateNewSessionID(ubody.Username)
	su := &defs.SignedUp{
		Success:   true,
		SessionID: id,
	}
	c.IndentedJSON(200, su)
}
