package dbops

import (
	"fmt"
	"testing"
	"time"
)

func TestMain(m *testing.M) {

	m.Run()
}
func TestVideoWorkFlow(t *testing.T) {
	t.Run("Add", testAddVideo)
	t.Run("Get", testGetVideo)
	t.Run("Delete", testDeleteVideo)
}

func TestUserWorkFlow(t *testing.T) {
	t.Run("Add", testAddUserCredential)
	t.Run("Get", testGetUser)
	t.Run("Delete", testDeleteUser)
	t.Run("Reget", testRegetUser)
}
func TestCommentsWorkFlow(t *testing.T) {
	t.Run("AddUser", testAddUserCredential)
	t.Run("AddComments", testAddComment)
	t.Run("ListComments", testListComments)
}
func testAddUserCredential(t *testing.T) {
	err := AddUserCredential("pitfall", "123")
	if err != nil {
		t.Errorf("Error of AddUser %v", err)
	}
}
func testGetUser(t *testing.T) {
	_, err := GetUserCredential("pitfall")
	if err != nil {
		t.Errorf("Error of getUser %v", err)
	}

}
func testDeleteUser(t *testing.T) {
	err := DeleteUser("pitfall", "123")
	if err != nil {
		t.Errorf("Error of delete User %v", err)
	}
}
func testRegetUser(t *testing.T) {
	_, err := GetUserCredential("pitfall")
	if err == nil {
		t.Errorf("Error of reget User")
	}
}

var tempvid string

func testAddVideo(t *testing.T) {
	video, _ := AddNewVideo(1, "my-video")

	tempvid = video.ID
}
func testGetVideo(t *testing.T) {
	_, err := GetVideoInfo(tempvid)
	if err != nil {
		t.Errorf("Error of getvideo")
	}

}
func testDeleteVideo(t *testing.T) {
	DeleteVideoInfo(tempvid)
}
func testAddComment(t *testing.T) {
	vid := "123"
	aid := uint(1)
	content := "i like this video"
	err := AddNewComment(vid, aid, content)
	if err != nil {
		t.Errorf("Error of AddComments %v", err)
	}
}
func testListComments(t *testing.T) {
	vid := "123"
	layout := "Jan 02 2006, 15:04:05"
	// 加载时区
	loc, err := time.LoadLocation("Asia/Shanghai") //一定要这个否则会弄错的
	from, _ := time.ParseInLocation(layout, "Dec 14 2021, 20:18:15", loc)
	fromUnix := from.Unix()
	fmt.Println(from)
	fmt.Println(fromUnix)
	to := time.Now()
	toUnix := to.Unix()
	fmt.Println(to)
	fmt.Println(toUnix)
	comments, err := ListComments(vid, fromUnix, toUnix)
	if err != nil {
		t.Errorf("Error of list comments:%v", err)
	}
	for _, val := range comments {
		fmt.Printf("%#v", val)
	}

}
