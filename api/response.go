package main

import (
	"GoStreamVideoWebsite/api/defs"
	"github.com/gin-gonic/gin"
)

func sendErrorResponse(c *gin.Context, errs defs.ErrorResponse) {
	c.IndentedJSON(errs.HttpSC, errs)
}
func sendNormalResponse(c *gin.Context, resp string, sc int) {
	c.IndentedJSON(sc, resp)
}
