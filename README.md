# 流媒体网站架构和API模块的实现

## 总体架构

![image-20211212170423850](D:\GoWorkspace\GoStreamVideoWebsite\README.assets\image-20211212170423850.png)



## API设计

![image-20211212192228687](D:\GoWorkspace\GoStreamVideoWebsite\README.assets\image-20211212192228687.png)

## Stream模块

### Streaming

+ 静态视频，像b站之类的，不是RTMP （real time message protocol 不是实时视频 如直播等
+ 独立的服务，可独立部署
+ 统一的api格式

### stream Server

+ streaming部分 用于输出视频
+ upload files 上传视频

## scheduler模块

![image-20211220140516612](D:\GoWorkspace\GoStreamVideoWebsite\README.assets\image-20211220140516612.png)

主要是用于异步的任务，比如说我删除一个视频，他就放入消息队列一样，过一段时间再去处理，不会立即处理。**采用生产者消费者模式**

