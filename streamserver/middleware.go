package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func RateLimiterMiddleware() gin.HandlerFunc {
	return func(context *gin.Context) {
		if !DefaultConnLimiter.GetConn() {
			sendErrorResponsr(context, http.StatusTooManyRequests, "Too many Request")
			context.Abort()
			return
		}
		defer DefaultConnLimiter.ReleaseConn()
	}
}
