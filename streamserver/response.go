package main

import "github.com/gin-gonic/gin"

func sendErrorResponsr(c *gin.Context, sc int, errMsg string) {
	c.IndentedJSON(sc, errMsg)
}
