package main

var (
	VIDEO_DIR       string = "./videos/"      //video 的放置目录 在pro的根目录下面的videos文件夹
	VIDEO_SUFFIX           = ".mp4"           //video的后缀 //这里改成用于windows的后缀，在linux环境下可能要修改
	MAX_UPLOAD_SIZE int64  = 1024 * 1024 * 50 //50MB的大小
)

//func init() {
//	dir, _ := os.Getwd()
//	VIDEO_DIR=dir+"/videos/"
//
//}
