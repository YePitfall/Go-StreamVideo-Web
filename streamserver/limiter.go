package main

import "log"

//防止连接过多导致服务器crash掉
//实现token bucket 令牌桶的限流
//这里使用channel 来避免枷锁的问题，否则我们需要加锁来判断，防止同时读的时候出错
//而且视频的输出是持续性的连接，建立了TCP连接，需要过一段时间释放，不像普通的数据库操作
type ConnLimiter struct {
	concurrentConn int
	bucket         chan int
}

var (
	DefaultConnLimiter *ConnLimiter
	DefaultConnection  = 2 //默认的最大连接数量
)

func init() {
	DefaultConnLimiter = NewConnLimiter(DefaultConnection)
}

func NewConnLimiter(cc int) *ConnLimiter {
	return &ConnLimiter{
		concurrentConn: cc,
		bucket:         make(chan int, cc),
	}
}
func (cl *ConnLimiter) GetConn() bool {
	if len(cl.bucket) >= cl.concurrentConn {
		log.Printf("Reached the rate limitation.")
		return false
	}
	cl.bucket <- 1
	return true
}
func (cl *ConnLimiter) ReleaseConn() {
	c := <-cl.bucket //可以把这个注释掉来测试效果
	log.Printf("Release a connection %d", c)
}
